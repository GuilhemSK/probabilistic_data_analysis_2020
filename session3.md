# Research seminar on probabilistic data analysis: Session 3 (April 17, 2020)

  * [Zoom link](zoom.md)
  * [Zulip chat](https://utu.zulipchat.com/#narrow/stream/230589-bda)

  * notes at [exercises/session3/](exercises/session3/)


# Discussion topic 1 (Aaro Salosensaari): Laplace approximation and Normal asymptotics

 * Chapters 4.1-4.4

Mandatory reading:
 * Read chapter 4 sections 4.1-4.5
 * Look at demo 4.1: https://github.com/aa-m-sa/BDA_R_demos/tree/master/demos_ch4
 * Let's try to focus on intuitions and consequences of theorems.

Exercises: (highly recommended)
 * Think about exercise 4.4. (related to section 4.2). If stuck, look at solutions manual linked below.

Additional reading, not part of the "official programme" but can be discussed if time permits:
* Appendix B. (recommended)
* Chapter 13. (for those curious about connections to more elaborate variational approximations)
    * if there is interest, I suggest we can cover variational inference in more detail later. 

# Discussion topic 2 (Felix Vaura): Relation to other statistical methods

 * Chapter 4.5
 
 Mandatory reading:
 * Chapter 4.5 with emphasis is on the following 3 subsections: 
   - Maximum likelihood and other point estimates
   - Confidence intervals
   - Multiple comparisons and multilevel modeling
 
 Exercises: 
 * Think about 4.11. and take a look the solution at:
   http://www.stat.columbia.edu/~gelman/book/solutions3.pdf (or solve it yourself!)
 * Be prepared to discuss 4.13.
 
 Strongly recommended (optional) reading:
 * Bayes vs. frequentist in a real-life drug trial example:
   https://www.fharrell.com/post/bayes-freq-stmts/ (thanks to Aaro for finding this).
 * Skim through the numbered paragraphs 1-25 in the following wonderful article: 
   https://link.springer.com/article/10.1007/s10654-016-0149-3 (and you’ll never 
   want to touch P values again).
 
 Optional (still recommended) reading:
 * Maximum likelihood and other point estimates (posteriors always win):
   https://towardsdatascience.com/mle-map-and-bayesian-inference-3407b2d6d4d9
 * Confidence intervals (an absolutely delightful answer by Keith Winstein):
   https://stats.stackexchange.com/questions/2272/whats-the-difference-between-a-confidence-interval-and-a-credible-interval
 * Multiple comparisons and multilevel modeling (by Andrew Gelman):
   https://statmodeling.stat.columbia.edu/2016/08/22/bayesian-inference-completely-solves-the-multiple-comparisons-problem/
   

### Exercises

Some code for the discussed assignments [here](exercises/).

