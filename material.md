## Source material

Study material for the [reading circle](README.md).

We will initially follow the [course setup by Aki
Vehtari](https://github.com/avehtari/BDA_course_Aalto), while adapting
according to the participants and the local context.

  * [Bayesian Data Analysis](http://www.stat.columbia.edu/~gelman/book/) (Gelman et al. 3rd edition)
  * [BDA R demos](https://github.com/avehtari/BDA_R_demos) (by Aki Vehtari et al.)
  * [BDA RStan demos](https://github.com/avehtari/BDA_R_demos/tree/master/demos_rstan) (by Aki Vehtari et al.)
  * [Model solutions to exercises](http://www.stat.columbia.edu/~gelman/book/solutions3.pdf)
  * [Assignments](https://github.com/avehtari/BDA_course_Aalto/tree/master/exercises)

### Supporting material 

Feel free to use also any other supporting resources. For instance:

  * [BDA Errata](http://www.stat.columbia.edu/~gelman/book/errata_bda3.txt)
  * [Statistical rethinking](https://github.com/rmcelreath/statrethinking_winter2019)


## Other material

[Ideas for additional sessions](extra.md) - to go through after or in
between the book chapters.