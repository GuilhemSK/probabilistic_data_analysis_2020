# Research seminar on probabilistic data analysis: Session 4 (May 1, 2020)

  * [Zoom link](zoom.md)
  * [Zulip chat](https://utu.zulipchat.com/#narrow/stream/230589-bda)


# Discussion topic 1 (Guilhem Sommeria-Klein): Hierarchical models

Main reading:

 * BDA Book Chapter 5

Optionally, you can also check:

 * [Slides from Vehtari](https://github.com/avehtari/BDA_course_Aalto/blob/master/slides/slides_ch5.pdf)
 * [Demos](https://github.com/avehtari/BDA_R_demos/tree/master/demos_ch5)


# Discussion topic 2 (Paul-Christian Burkner): brms R package

Main reading covers the two main brms papers:

 * [brms: An R Package for Bayesian Multilevel Models Using Stan](https://www.jstatsoft.org/article/view/v080i01)
 * [Advanced Bayesian Multilevel Modeling with the R Package brms](https://journal.r-project.org/archive/2018/RJ-2018-017/index.html)



