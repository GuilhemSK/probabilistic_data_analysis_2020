
# Research seminar on probabilistic data analysis: Session 1

We are initially following the excellent [course setup designed by Aki
Vehtari](https://github.com/avehtari/BDA_course_Aalto), while adapting
according to the participants and the local context.

This page shows the content for session 1. Check also the [overall
schedule](schedule.md).


### Chapter 1

The material from chapter 1 is optional background that gives some basics of Bayesian thinking.

 * [BDA Chapter 1](https://statisticalsupportandresearch.files.wordpress.com/2017/11/bayesian_data_analysis.pdf)
 * [Notes on Chapter 1](https://github.com/avehtari/BDA_course_Aalto/tree/master/chapter_notes)
 * [Slides](https://github.com/avehtari/BDA_course_Aalto/tree/master/slides); bayes_intro.pdf and slides_extra.pdf
 * [Model solutions to exercises in chapter 1](http://www.stat.columbia.edu/~gelman/book/solutions3.pdf)
 * [Video lectures](https://aalto.cloud.panopto.eu/Panopto/Pages/Sessions/List.aspx#folderID=%22f0ec3a25-9e23-4935-873b-a9f401646812%22 ) (BDA course and BDA Lectures for chapters 1-2)

### Chapter 2 (Main)

This is the main chapter for the first session.

 * [BDA Chapter 2](https://statisticalsupportandresearch.files.wordpress.com/2017/11/bayesian_data_analysis.pdf)
 * [Slides (chapter 2)](https://github.com/avehtari/BDA_course_Aalto/tree/master/slides)
 * [BDA demos (chapter 2)](https://github.com/avehtari/BDA_R_demos)
 * [Model solutions to exercises in chapter 2](http://www.stat.columbia.edu/~gelman/book/solutions3.pdf)
 * Think (and try?) [assignment 2](https://github.com/avehtari/BDA_course_Aalto/blob/master/exercises/ex2.pdf)

### Exercises

Some code for the discussed assignments [here](exercises/).

