# Research seminar on probabilistic data analysis: Session 2

## Background reading before the session
 - Read chapters 2-3
 - Check demos for chapters 2-3: http://avehtari.github.io/BDA_R_demos

## Optional material
 - Consider checking the following for chapters 2-3:
 - Vehtari’s slides for chapters 2-3: https://github.com/avehtari/BDA_course_Aalto/tree/master/slides
 - Exercises https://github.com/avehtari/BDA_course_Aalto/tree/master/exercises
 - Notes https://github.com/avehtari/BDA_course_Aalto/tree/master/chapter_notes

# Discussion theme 1 (Ville Laitinen): Priors

 - Check characteristics of the different prior types in chapters 2-3 (conjugate, non-informative, proper and improper, weakly informative & informative, prior sensitivity) for binomial and normal models.

 - Stan development team’s prior choice recommendations (good reference): https://github.com/stan-dev/stan/wiki/Prior-Choice-Recommendations 
 
 - Bonus material
    - [Should we about rigged priors?](https://statmodeling.stat.columbia.edu/2017/10/04/worry-rigged-priors/)

# Discussion theme 2 (Leo Lahti): Stan

We will first get started with the rstanarm package. This provides simplified Stan shortcuts for many standard models. 

Check at least the following documentation and case studies that will be discussed in more detail during the second half of the seminar session.
 - Documentation for rstanarm (Stan shortcuts): http://mc-stan.org/rstanarm/index.html
 - Estimating GLMs: https://mc-stan.org/rstanarm/articles/continuous.html
 - Probabilistic versions for standard  models: https://lindeloev.github.io/tests-as-linear/ 

Optionally, you can also check further background reading & self-study material for Stan (we will get back to Stan later):
 - Formal publication of Stan: www.stat.columbia.edu/~gelman/research/published/Stan-paper-aug-2015.pdf
 - Documentation for Stan:  https://mc-stan.org/users/documentation/
 - Videos: basics of Bayesian inference and Stan (Jonah Gabry & Lauren Kennedy):
    - Part I: https://www.youtube.com/watch?v=ZRpo41l02KQ&t=8s&list=PLuwyh42iHquU4hUBQs20hkBsKSMrp6H0J&index=6
    - Part II: https://www.youtube.com/watch?v=6cc4N1vT8pk&t=0s&list=PLuwyh42iHquU4hUBQs20hkBsKSMrp6H0J&index=7


### Exercises

Some code for the discussed assignments [here](exercises/).

